package com.kado.one.shafu.informationsecurity.domain

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.security.MessageDigest

object LSystem {
    fun sha512(input: String, charset: Charset = StandardCharsets.UTF_8) =
        getHash("SHA-512", input.toByteArray(charset))

    fun sha512(input: ByteArray) = getHash("SHA-512", input)

    fun sha256(input: String, charset: Charset = StandardCharsets.UTF_8) =
        getHash("SHA-256", input.toByteArray(charset))

    fun sha256(input: ByteArray) = getHash("SHA-256", input)

    fun sha1(input: String, charset: Charset = StandardCharsets.UTF_8) = getHash("SHA-1", input.toByteArray(charset))
    fun sha1(input: ByteArray) = getHash("SHA-1", input)

    /**
     * Supported algorithms on Android:
     *
     * Algorithm	Supported API Levels
     * MD5          1+
     * SHA-1	    1+
     * SHA-224	    1-8,22+
     * SHA-256	    1+
     * SHA-384	    1+
     * SHA-512	    1+
     */
    private fun getHash(type: String, input: ByteArray): ByteArray = MessageDigest.getInstance(type).digest(input)
}