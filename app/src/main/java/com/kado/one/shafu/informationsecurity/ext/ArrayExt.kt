package com.kado.one.shafu.informationsecurity.ext

import java.nio.ByteBuffer

fun ByteArray.toHexString(): String {
    val hexChars = "0123456789ABCDEF"
    val result = StringBuilder(this.size * 2)

    this.forEach {
        val i = it.toInt()
        result.append(hexChars[i shr 4 and 0x0f])
        result.append(hexChars[i and 0x0f])
    }
    return result.toString()
}

fun ByteArray.toIntArray(): IntArray {
    if (this.size % Integer.BYTES != 0) {
        throw IllegalArgumentException("Byte array length")
    }
    val buf = ByteBuffer.wrap(this)

    val result = IntArray(this.size / Integer.BYTES)
    for (i in result.indices) {
        result[i] = buf.int
    }
    return result
}

fun IntArray.toByteArray(): ByteArray {
    val buf = ByteBuffer.allocate(this.size * Integer.BYTES)
    for (i in this.indices)
        buf.putInt(this[i])
    return buf.array()
}