package com.kado.one.shafu.informationsecurity.domain

import com.kado.one.shafu.informationsecurity.ext.toByteArray
import com.kado.one.shafu.informationsecurity.ext.toIntArray
import java.nio.ByteBuffer
import java.nio.charset.Charset


class Sha256(private val charset: Charset) {

    /**
     * Хэширует данное сообщение с SHA-256 и возвращает хеш.
     */
    fun hash(message: String) = hash(message.toByteArray(charset))

    /**
     * Хэширует данное сообщение с SHA-256 и возвращает хеш.
     */
    fun hash(message: ByteArray): ByteArray {
        val words = IntArray(64)
        val hash = IntArray(8)
        val tmp = IntArray(8)

        // H = H0
        System.arraycopy(H0, 0, hash, 0, H0.size)

        // Первичная обработка:
        val intWords = pad(message).toIntArray()

        // Обработать все блоки по 512 бит, каждый блок содержит 16 слов длиной 32 бита:
        val countBlocks = intWords.size / 16
        repeat(countBlocks) { iBlock ->
            // Сгенерировать дополнительные 48 слов:
            System.arraycopy(intWords, iBlock * 16, words, 0, 16)
            for (j in 16 until words.size) {
                words[j] = s1(words[j - 2]) + words[j - 7] + s0(words[j - 15]) + words[j - 16]
            }

            // tmp = H
            System.arraycopy(hash, 0, tmp, 0, hash.size)

            // Основной цикл:
            repeat(words.size) {
                val t1 = tmp[7] + sigma1(tmp[4]) + ch(tmp[4], tmp[5], tmp[6]) + K[it] + words[it]
                val t2 = sigma0(tmp[0]) + maj(tmp[0], tmp[1], tmp[2])
                System.arraycopy(tmp, 0, tmp, 1, tmp.size - 1)
                tmp[4] += t1
                tmp[0] = t1 + t2
            }

            // Добавить полученные значения к ранее вычисленному результату:
            repeat(hash.size) {
                hash[it] += tmp[it]
            }
        }
        // Получить итоговое значение хеша:
        return hash.toByteArray()
    }

    /**
     * Дополняет заданное сообщение длиной кратной 512 битам (64 байта),
     * включая добавление единичного бита, k нулевых бит и длины сообщения в виде 64-битного целого числа.
     */
    fun pad(message: ByteArray): ByteArray {
        val blockBits = 512
        val blockBytes = blockBits / 8

        // Новая длинна сообщения: original + 1-bit + 8-byte padding length
        var newMessageLength = message.size + 1 + 8
        val padBytes = blockBytes - newMessageLength % blockBytes
        newMessageLength += padBytes

        // Копируем старое сообщение в новый расширенный массив
        val paddedMessage = ByteArray(newMessageLength)
        System.arraycopy(message, 0, paddedMessage, 0, message.size)

        // Записываем 1-bit
        paddedMessage[message.size] = 128.toByte()

        // Пропускаем @padBytes, при инициализации @paddedMessage они уже равны нулю

        // Записываем 8-байтовое целое число, описывающее исходную длину сообщения
        val lenPos = message.size + 1 + padBytes
        ByteBuffer.wrap(paddedMessage, lenPos, 8).putLong(message.size * 8L)

        return paddedMessage
    }

    companion object {

        fun ch(x: Int, y: Int, z: Int): Int = (x and y) xor (x.inv() and z)

        fun maj(x: Int, y: Int, z: Int): Int = (x and y) xor (x and z) xor (y and z)

        fun sigma0(x: Int): Int = rotr(x, 2) xor rotr(x, 13) xor rotr(x, 22)

        fun sigma1(x: Int): Int = rotr(x, 6) xor rotr(x, 11) xor rotr(x, 25)

        fun s0(x: Int): Int = rotr(x, 7) xor rotr(x, 18) xor x.ushr(3)

        fun s1(x: Int): Int = rotr(x, 17) xor rotr(x, 19) xor x.ushr(10)

        private fun rotr(x: Int, dist: Int): Int = Integer.rotateRight(x, dist)

        // первые 32 бита дробных частей кубических корней первых 64 простых чисел [от 2 до 311]
        private val K = Constants.K
        // первые 32 бита дробных частей квадратных корней первых восьми простых чисел [от 2 до 19]
        private val H0 = Constants.H0
    }
}