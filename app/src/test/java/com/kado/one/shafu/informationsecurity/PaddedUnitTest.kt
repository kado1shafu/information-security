package com.kado.one.shafu.informationsecurity

import com.kado.one.shafu.informationsecurity.domain.Sha256
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.nio.charset.StandardCharsets


class PaddedUnitTest {
    @Test
    fun testPaddedLengthDivisibleBy512() {
        val b = byteArrayOf(0, 1, 2, 3, 0)

        val padded = Sha256(StandardCharsets.UTF_8).pad(b)
        val paddedLengthBits = padded.size * 8

        assertTrue(paddedLengthBits % 512 == 0)
    }

    @Test
    fun testPaddedMessageHas1Bit() {
        val b = ByteArray(64)

        val padded = Sha256(StandardCharsets.UTF_8).pad(b)

        assertEquals(128.toByte(), padded[b.size])
    }

    @Test
    fun testPaddingAllZero() {
        val b = byteArrayOf(1, 1, 1, 1, 1, 1, 1)

        val padded = Sha256(StandardCharsets.UTF_8).pad(b)

        for (i in b.size + 1 until padded.size - 8) {
            assertEquals("byte $i not 0", 0.toByte(), padded[i])
        }
    }
}
