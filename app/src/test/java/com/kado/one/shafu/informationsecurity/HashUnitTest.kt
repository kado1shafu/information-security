package com.kado.one.shafu.informationsecurity

import com.kado.one.shafu.informationsecurity.domain.LSystem
import com.kado.one.shafu.informationsecurity.domain.Sha256
import com.kado.one.shafu.informationsecurity.ext.toHexString
import org.junit.Assert.assertArrayEquals
import org.junit.Before
import org.junit.Test
import java.nio.charset.StandardCharsets


class HashUnitTest {

    private lateinit var hashGenerator: Sha256

    @Before
    fun init() {
        hashGenerator = Sha256(StandardCharsets.UTF_8)
    }

    @Test
    fun testHashRawBytes() {
        val message = ByteArray(256)
        repeat(message.size) {
            message[it] = it.toByte()
        }

        val expected = LSystem.sha256(message)
        val actual = hashGenerator.hash(message)

        assertArrayEquals(expected, actual)
        println(actual.toHexString())
    }

    @Test
    fun testHashEmpty() {
        val message = byteArrayOf()
        val expected = LSystem.sha256(message)
        val actual = hashGenerator.hash(message)

        assertArrayEquals(expected, actual)
        println(actual.toHexString())
    }

    @Test
    fun testHashRegular() {
        val message = "Hello world!"
        val expected = LSystem.sha256(message)
        val actual = hashGenerator.hash(message)

        assertArrayEquals(expected, actual)
        println(actual.toHexString())
    }

    @Test
    fun testHashLong() {
        val message = """Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.
            Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий
            безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки
            образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул
            в электронный дизайн.""".trimMargin()

        val expected = LSystem.sha256(message)
        val actual = hashGenerator.hash(message)

        assertArrayEquals(expected, actual)
        println(actual.toHexString())
    }
}